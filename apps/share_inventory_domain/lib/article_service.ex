defmodule Share.Inventory.Domain.ArticleService do
  @moduledoc """
    Provide an article services

  """

  @type list_of_articles :: [Article]

  @doc """
    Get all the articles from a userid

    ##Example

    iex> ArticleService.allArticles(1)
    []

  """

  @spec allArticles(userid: pid) :: list_of_articles
  def allArticles(_userId) do
    []
  end
end
