Feature:
  In order to know the article I own
  I want a mean to CRUD them
  As a register user

  Scenario: Display an empty list
    Given I don't own any article
    When I ask for the list
    Then the system display an empty list

#  Scenario: Display a list of articles I own
#    Given I own a list of article
#    When I ask for the list
#    Then the system display the list of articles
#
#  Scenario:  Create an articles
#    Given I want to create a new article
#    When I enter the article attributes: name, description, tag
#    And I press Save
#    Then the system display the list of articles
