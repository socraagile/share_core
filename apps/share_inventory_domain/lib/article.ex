defmodule Article do
  @moduledoc false
  defstruct [:articleId, :userId, :name, :tags, :status]
end
