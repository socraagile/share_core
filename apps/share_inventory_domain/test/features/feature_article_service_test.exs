defmodule Share.Inventory.Domain.FeatureArticleServiceTest do
  use Cabbage.Feature, async: false, file: "article.feature"
  use ExUnit.Case

  alias Share.Inventory.Domain.ArticleService

  @moduletag :capture_log

  # `setup_all/1` provides a callback for doing something before the entire suite runs
  # As below, `setup/1` provides means of doing something prior to each scenario
  setup do
    on_exit fn -> # Do something when the scenario is done
      IO.puts "Scenario completed, cleanup stuff"
    end
    %{my_state: :state}
  end

  defgiven ~r/^I don't own any article$/, _vars, _state do
    {:ok, %{userid: 999, message: nil}}
  end

  defwhen ~r/^I ask for the list$/, _wars, state do
    {:ok, %{articles: ArticleService.allArticles(state.userid)}}
  end

  defthen ~r/^the system display an empty list$/, _vars, state do
    assert length(state.articles) == 0
  end

end
