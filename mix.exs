defmodule ShareCore.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:mix_test_watch, "~> 0.9.0", only: :dev, runtime: false},
      {:cabbage, "~> 0.3.0", only: :test},
      {:mox, "~> 0.5", only: [:test, :dev]},
      {:ecto, "~> 3.0"}
    ]
  end
end
