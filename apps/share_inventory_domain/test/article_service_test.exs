defmodule Share.Inventory.Domain.ArticleServiceTest do
  use ExUnit.Case

  alias Share.Inventory.Domain.ArticleService

  @moduletag :capture_log

  doctest ArticleService

  test "should return an empty list of Articles if there is no articles" do
    userid = Integer.to_string(:rand.uniform(4294967296), 32) <> Integer.to_string(:rand.uniform(4294967296), 32)

    result = ArticleService.allArticles(userid)

    assert is_list(result)
    assert length(result) == 0
  end
end
