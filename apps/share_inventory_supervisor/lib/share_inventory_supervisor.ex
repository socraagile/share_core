defmodule ShareInventorySupervisor do
  @moduledoc """
  Documentation for ShareInventorySupervisor.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ShareInventorySupervisor.hello()
      :world

  """
  def hello do
    :world
  end
end
